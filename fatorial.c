#include "fatorial.h"

int fatorial(int x){
	if(x == 1) {
		return 1;
	} else if (x > 1) {
		return(x * fatorial(x-1));
	}
	return -1;
}